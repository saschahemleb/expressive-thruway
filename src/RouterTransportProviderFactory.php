<?php
namespace Zend\Expressive\Thruway;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Thruway\Transport\RatchetTransportProvider;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class RouterTransportProviderFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $transportConfig = isset($config['thruway']['router']['transports'][$requestedName])
            ? $config['thruway']['router']['transports'][$requestedName]
            : array();

        return $this->initTransport($transportConfig, $requestedName);
    }

    private function initTransport(array $config, $transportName) {
        switch ($transportName) {
            case RatchetTransportProvider::class:
                return $this->initRatchet($config);

            default:
                throw new \InvalidArgumentException(sprintf('Unknown transport provider "%s"', $transportName));
        }
    }

    private function initRatchet(array $config) {
        $address = !empty($config['address']) ? $config['address'] : "127.0.0.1";
        $port = !empty($config['port']) ? $config['port'] : 9090;

        return new RatchetTransportProvider($address, $port);
    }
}