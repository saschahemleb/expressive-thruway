<?php
namespace Zend\Expressive\Thruway;

use Thruway\ClientSession;

interface ProviderInterface
{
  public function initClientSession(ClientSession $session);
}