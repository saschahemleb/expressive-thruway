<?php
namespace Zend\Expressive\Thruway;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Thruway\Peer\Router;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class RouterFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $transports = $config['thruway']['router']['transports'];
        $router = new Router();

        foreach (array_keys($transports) as $transport) {
            $router->addTransportProvider($container->get($transport));
        }

        return $router;
    }
}