<?php
namespace Zend\Expressive\Thruway;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Thruway\Transport\PawlTransportProvider;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class ClientTransportProviderFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $transportConfig = isset($config['thruway']['client']['transports'][$requestedName])
            ? $config['thruway']['client']['transports'][$requestedName]
            : array();

        return $this->initTransport($transportConfig, $requestedName);
    }

    private function initTransport(array $config, $transportName) {
        switch ($transportName) {
            case PawlTransportProvider::class:
                return $this->initPawl($config);

            default:
                throw new \InvalidArgumentException(sprintf('Unknown transport provider "%s"', $transportName));
        }
    }

    private function initPawl(array $config) {
        if (empty($config['url'])) {
            throw new \InvalidArgumentException(sprintf('Transport %s requires the url option', PawlTransportProvider::class));
        }

        $url = $config['url'];
        return new PawlTransportProvider($url);
    }
}