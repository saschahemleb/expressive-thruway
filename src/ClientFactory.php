<?php
namespace Zend\Expressive\Thruway;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Psr\Log\NullLogger;
use Thruway\ClientSession;
use Thruway\Logging\Logger;
use Thruway\Peer\Client;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class ClientFactory implements FactoryInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $this->container = $container;

        $config = $container->get('config');
        $transports = $config['thruway']['client']['transports'];
        Logger::set(new NullLogger());
        $client = new Client($config['thruway']['client']['realm']);

        foreach (array_keys($transports) as $transport) {
            $client->addTransportProvider($container->get($transport));
        }

        $client->once('open', [$this, 'onOpen']);

        return $client;
    }

    public function onOpen(ClientSession $session)
    {
        $config = $this->container->get('config');
        $providers = $config['thruway']['providers'];

        foreach ($providers as $provider) {
            if ($this->container->has($provider)) {
                $provider = $this->container->get($provider);
            }

            if (!($provider instanceof ProviderInterface)) {
                throw new \InvalidArgumentException(sprintf('Provider must be an instance of %s, got %s',
                    ProviderInterface::class, get_class($provider)));
            }

            $provider->initClientSession($session);
        }
    }
}